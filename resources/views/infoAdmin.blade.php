<!doctype html>
<html lang="en">
  <head>
    <title>Info - Rental Kamera</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Material Dashboard CSS -->
    <link rel="stylesheet" href="{{asset('/assets/css/material-dashboard.css')}}">

  </head>
  <body class="dark-edition">
    <style>
      .hitam{background: #1A2035 !important};
      .itam{background-color: #1A2035 !important};
  </style>

    <div class="wrapper ">
      <div class="sidebar" data-color="danger" data-background-color="danger" data-image="../assets/img/sidebar-2.jpg">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
  
          Tip 2: you can also add an image using data-image tag
      -->
        <div class="logo"><a href="./index" class="simple-text logo-normal">
            Rental Kamera Suka Suka
          </a></div>
        <div class="sidebar-wrapper">
          <ul class="nav">
            <li class="nav-item  ">
              <a class="nav-link text-white" href="./index">
                
                <h4>Dashboard</h4>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link text-white" href="./penyewaan">
                
                <h4>Penyewaan</h4>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link text-white" href="./tabel">
                
                <h4>Tabel Pelanggan</h4>
              </a>
            </li>
            <li class="nav-item active ">
              <a class="nav-link text-white" href="./infoAdmin">                
              <h4>Info Admin</h4>
              </a>
            </li>                        
          </ul>
        </div>
      </div>
      <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-md hitam itam">
          <div class="container hitam itam">
             
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                  <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <!-- Left Side Of Navbar -->
                  <ul class="navbar-nav mr-auto">

                  </ul>

                  <!-- Right Side Of Navbar -->
                  <ul class="navbar-nav ml-auto">
                      <!-- Authentication Links -->
                      @guest
                          @if (Route::has('login'))
                              <li class="nav-item">
                                  <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                              </li>
                          @endif

                          @if (Route::has('register'))
                              <li class="nav-item">
                                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                              </li>
                          @endif
                      @else
                          <li class="nav-item dropdown">
                              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  {{ Auth::user()->name }}
                              </a>

                              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                      {{ __('Logout') }}
                                  </a>

                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                      @csrf
                                  </form>
                              </div>
                          </li>
                      @endguest
                  </ul>
              </div>
          </div>
      </nav>
        <!-- End Navbar -->
        <div class="content">
          <div class="container-fluid">
            <div class="card">
              <div class="card-header card-header-danger">
                <h4 class="card-title">Informasi Admin</h4>                
              </div>
              <div class="card-body mt-4">                
                  <div class="card card-profile">
                    <div class="card-avatar">
                      <a href="#pablo">
                        <img class="img" src="../assets/img/faces/marc.jpg" />
                      </a>
                    </div>
                    <div class="card-body">
                      <h6 class="card-category">Admin Rental Suka Suka</h6>
                      <h4 class="card-title">Yusril Izha Mahendra</h4>
                      <p class="card-description">
                        Jl. Panglima Polem No.07 <br>
                        Besusu Barat, Kec. Palu Timur, Kota Palu, Sulawesi Tengah 94118 <br>
                        No. Hp : 085342772430
                      </p>
                      <a href="https://wa.me/6282290489097" class="btn btn-danger btn-round">Hubungi</a>
                    </div>
                  </div>                
              </div>
            </div>
          </div>
        </div>       
        <script>
          const x = new Date().getFullYear();
          let date = document.getElementById('date');
          date.innerHTML = '&copy; ' + x + date.innerHTML;
        </script>
      </div>
    </div>
         
   
    
  </body>
    

    



    <!--   Core JS Files   -->
    <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-material-design.js')}}"></script>

    <script src="https://unpkg.com/default-passive-events"></script>

    <!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
    <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>

    <!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
    <script src="{{asset('assets/js/core/chartist.min.js')}}"></script>

    <!-- Plugin for Scrollbar documentation here: https://github.com/utatti/perfect-scrollbar -->
    <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>


    <!-- Demo init -->
    <script src="{{asset('assets/js/plugins/demo.js')}}"></script>

    <!-- Material Dashboard Core initialisations of plugins and Bootstrap Material Design Library -->
    <script src="{{asset('assets/js/material-dashboard.js?v=2.1.0')}}"></script>
  </body>
</html>
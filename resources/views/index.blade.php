<!doctype html>
<html lang="en">
  <head>
    <title>Dashboard - Rental Kamera</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Material Dashboard CSS -->
    <link rel="stylesheet" href="{{asset('/assets/css/material-dashboard.css')}}">

  </head>
  <style>
    .hitam{background: #1A2035 !important};
    .itam{background-color: #1A2035 !important};
</style>
  
  <body class="dark-edition">
    

    <body class="dark-edition">
        
        <div class="wrapper ">
          <div class="sidebar" data-color="azure" data-background-color="black" data-image="{{asset('../assets/img/sidebar-2.jpg')}}">
            <!--
              Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
      
              Tip 2: you can also add an image using data-image tag
          -->
            <div class="logo"><a href="./index" class="simple-text logo-normal">
                Rental Kamera Suka Suka
              </a></div>
            <div class="sidebar-wrapper">
              <ul class="nav">
                <li class="nav-item active  ">
                  <a class="nav-link" href="./index">
                    
                    <h4>Dashboard</h4>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="./penyewaan">
                    
                    <h4>Penyewaan</h4>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="./tabel">
                    
                    <h4>Tabel Pelanggan</h4>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="./infoAdmin">
                    
                    <h4>Info Admin</h4>
                  </a>
                </li>                
              </ul>
            </div>
          </div>
          <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-md hitam itam">
                <div class="container">                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
    
                        </ul>
    
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                @if (Route::has('login'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                @endif
    
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }}
                                    </a>
    
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                          onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
    
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->

            <div class="content">
              <div class="container-fluid">             
                <div class="row">
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-stats">
                      <div class="card-header card-header-warning card-header-icon">
                        <div class="card-icon">
                          <i class="material-icons"></i>
                        </div>
                        <p class="card-category">Pelanggan </p>
                        <h3 class="card-title">4 Orang                          
                        </h3>
                      </div>
                      <div class="card-footer">
                        <div class="stats">                          
                          <a href="./tabel" class="warning-link">Menuju Ke Tabel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-stats">
                      <div class="card-header card-header-warning card-header-icon">
                        <div class="card-icon">
                          <i class="material-icons"></i>
                        </div>
                        <p class="card-category">Jumlah Kamera </p>
                        <h3 class="card-title">5 Item                          
                        </h3>
                      </div>
                      <div class="card-footer">
                        <div class="stats">                          
                          <a href="#kamera" class="warning-link">Lihat Tabel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-stats">
                      <div class="card-header card-header-danger card-header-icon">
                        <div class="card-icon">
                          <i class="material-icons"></i>
                        </div>
                        <p class="card-category">Admin</p>
                        <h4 class="card-title">Yusril Izha Mahendra</h4>
                      </div>
                      <div class="card-footer">
                        <div class="stats">
                          <a href="./infoAdmin" class="danger-link">Lihat Profil</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
                <div class="row"> 
                <div class="col-md-6" id="kamera"> 
                    <div class="card">
                      <div class="card-header card-header-primary">
                        <h4 class="card-title">Daftar Kamera</h4>                        
                      </div>
                      <div class="card-body table-responsive">
                        <table class="table table-hover">
                          <thead class="text-warning">
                            <th>ID</th>
                            <th>Nama Kamera</th>
                            <th>Harga Sewa/Hari</th>                            
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td>Sony</td>
                              <td>Rp.100.000</td>                              
                            </tr>
                            <tr>
                              <td>2</td>
                              <td>Nikkon</td>
                              <td>Rp.70.000</td>                              
                            </tr>
                            <tr>
                              <td>3</td>
                              <td>Canon</td>
                              <td>Rp.150.000</td>                              
                            </tr> 
                            <tr>
                                <td>4</td>
                                <td>Fuji Film</td>
                                <td>Rp.90.000</td>                              
                              </tr>                           
                          </tbody>
                        </table>
                      </div>
                    </div>
                <a href="#" button type="button"  class="btn btn-primary">Tambah Kamera</a>

                </div>  
                <div class="col-md-6">
                  <div class="card card-profile ">
                    <div class="card-avatar">
                      <a href="#pablo">
                        <img class="img" src="{{asset('../assets/img/kamera.png')}}" />
                      </a>
                    </div>
                    <div class="card-body mt-5 mb-4">                    
                      <h4 class="card-title">Rental Kamera Suka Suka</h4>
                      <p class="card-description">
                          Jl. Miangas IV No.10, Lolu Sel., Kec. Palu Tim., Kota Palu, Sulawesi Tengah 94111
                      </p>
                      <div class="mt-4">
                  

                    <i class="fa fa-instagram w-100"> Instagram</i>
                  
                      <a href="https://goo.gl/maps/9DxK4uC5ZGTBSAhP9" class="btn btn-primary btn-round ">Kunjungi</a>
                    </div>
                    </div>
                  </div>
                </div>
            </div>
                </div>                
              </div>
            </div>          
            <script>
              const x = new Date().getFullYear();
              let date = document.getElementById('date');
              date.innerHTML = '&copy; ' + x + date.innerHTML;
            </script>
          </div>
        </div>
        
        <!--   Core JS Files   -->
        <script src="../assets/js/core/jquery.min.js"></script>
        <script src="../assets/js/core/popper.min.js"></script>
        <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
        <script src="https://unpkg.com/default-passive-events"></script>
        <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!-- Place this tag in your head or just before your close body tag. -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Chartist JS -->
        <script src="../assets/js/plugins/chartist.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="../assets/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../assets/js/material-dashboard.js?v=2.1.0"></script>
        <!-- Material Dashboard DEMO methods, don't include it in your project! -->
        <script src="../assets/demo/demo.js"></script>
        <script>
          $(document).ready(function() {
            $().ready(function() {
              $sidebar = $('.sidebar');
      
              $sidebar_img_container = $sidebar.find('.sidebar-background');
      
              $full_page = $('.full-page');
      
              $sidebar_responsive = $('body > .navbar-collapse');
      
              window_width = $(window).width();
      
              $('.fixed-plugin a').click(function(event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                  if (event.stopPropagation) {
                    event.stopPropagation();
                  } else if (window.event) {
                    window.event.cancelBubble = true;
                  }
                }
              });
      
              $('.fixed-plugin .active-color span').click(function() {
                $full_page_background = $('.full-page-background');
      
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
      
                var new_color = $(this).data('color');
      
                if ($sidebar.length != 0) {
                  $sidebar.attr('data-color', new_color);
                }
      
                if ($full_page.length != 0) {
                  $full_page.attr('filter-color', new_color);
                }
      
                if ($sidebar_responsive.length != 0) {
                  $sidebar_responsive.attr('data-color', new_color);
                }
              });
      
              $('.fixed-plugin .background-color .badge').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
      
                var new_color = $(this).data('background-color');
      
                if ($sidebar.length != 0) {
                  $sidebar.attr('data-background-color', new_color);
                }
              });
      
              $('.fixed-plugin .img-holder').click(function() {
                $full_page_background = $('.full-page-background');
      
                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');
      
      
                var new_image = $(this).find("img").attr('src');
      
                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                  $sidebar_img_container.fadeOut('fast', function() {
                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $sidebar_img_container.fadeIn('fast');
                  });
                }
      
                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                  var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
      
                  $full_page_background.fadeOut('fast', function() {
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                    $full_page_background.fadeIn('fast');
                  });
                }
      
                if ($('.switch-sidebar-image input:checked').length == 0) {
                  var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                  var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
      
                  $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                  $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }
      
                if ($sidebar_responsive.length != 0) {
                  $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
              });
      
              $('.switch-sidebar-image input').change(function() {
                $full_page_background = $('.full-page-background');
      
                $input = $(this);
      
                if ($input.is(':checked')) {
                  if ($sidebar_img_container.length != 0) {
                    $sidebar_img_container.fadeIn('fast');
                    $sidebar.attr('data-image', '#');
                  }
      
                  if ($full_page_background.length != 0) {
                    $full_page_background.fadeIn('fast');
                    $full_page.attr('data-image', '#');
                  }
      
                  background_image = true;
                } else {
                  if ($sidebar_img_container.length != 0) {
                    $sidebar.removeAttr('data-image');
                    $sidebar_img_container.fadeOut('fast');
                  }
      
                  if ($full_page_background.length != 0) {
                    $full_page.removeAttr('data-image', '#');
                    $full_page_background.fadeOut('fast');
                  }
      
                  background_image = false;
                }
              });
      
              $('.switch-sidebar-mini input').change(function() {
                $body = $('body');
      
                $input = $(this);
      
                if (md.misc.sidebar_mini_active == true) {
                  $('body').removeClass('sidebar-mini');
                  md.misc.sidebar_mini_active = false;
      
                  $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
      
                } else {
      
                  $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');
      
                  setTimeout(function() {
                    $('body').addClass('sidebar-mini');
      
                    md.misc.sidebar_mini_active = true;
                  }, 300);
                }
      
                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function() {
                  window.dispatchEvent(new Event('resize'));
                }, 180);
      
                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function() {
                  clearInterval(simulateWindowResize);
                }, 1000);
      
              });
            });
          });
        </script>
        <script>
          $(document).ready(function() {
            // Javascript method's body can be found in assets/js/demos.js
            md.initDashboardPageCharts();
      
          });
        </script>
      </body>



    <!--   Core JS Files   -->
    <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-material-design.js')}}"></script>

    <script src="https://unpkg.com/default-passive-events"></script>

    <!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
    <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>

    <!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
    <script src="{{asset('assets/js/core/chartist.min.js')}}"></script>

    <!-- Plugin for Scrollbar documentation here: https://github.com/utatti/perfect-scrollbar -->
    <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>


    <!-- Demo init -->
    <script src="{{asset('assets/js/plugins/demo.js')}}"></script>

    <!-- Material Dashboard Core initialisations of plugins and Bootstrap Material Design Library -->
    <script src="{{asset('assets/js/material-dashboard.js?v=2.1.0')}}"></script>
  </body>
</html>